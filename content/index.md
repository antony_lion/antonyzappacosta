---
title: ""
date: 2023-06-22T07:55:41+02:00
---


<div style="display: flex; align-items: center; margin-top: 0;">

<!-- Set up a circular image with a border radius of 50% -->
<img src="/images/Antony.jpg" alt="Profile picture" style="border-radius: 50%; height: 250px; width: 250px; margin-right: 70px;">

<div> <h1>Antony Zappacosta</h1>
<h3 style="display: inline;">Software engineer</h3><h4 style="display: inline;"> - Munich, Germany</h4>
<h5 style="margin-top: 20px;">Computer Engineering graduate from the University of Bologna, Italy. Deeply passionate about software engineering and computer vision.</h5>
</div>
</div>